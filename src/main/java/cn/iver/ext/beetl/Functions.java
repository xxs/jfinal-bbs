package cn.iver.ext.beetl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.iver.common.WebUtils;
import cn.iver.model.User;

/**
 * beetl 自定义函数
 * @author L.cm
 * email: 596392912@qq.com
 * site:http://www.dreamlu.net
 * date 2015年7月4日下午6:05:07
 */
public class Functions {

	/**
	 * 继续encode URL (url,传参tomcat会自动解码)
	 * 要作为参数传递的话，需要再次encode
	 * @param encodeUrl
	 * @return String
	 */
	public String encodeUrl(String url) {
		try {
			url = URLEncoder.encode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// ignore
		}
		return url;
	}


	/**
	 * 模版中拿取cookie中的用户信息
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	public User currentUser(HttpServletRequest request, HttpServletResponse response) {
		return WebUtils.currentUser(request, response);
	}

}
