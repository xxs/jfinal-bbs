package cn.iver.controller;

import cn.iver.model.Topic;
import cn.iver.model.User;

import com.jfinal.core.Controller;

/**
 * Created with IntelliJ IDEA.
 */
public class IndexController extends Controller {
	
    public void index(){
    	setAttr("topicPage", Topic.dao.getPage(getParaToInt(0, 1)));
        setAttr("userList", User.dao.getUserListbyPoint(8));
        setAttr("topicList", Topic.dao.getHotTopinList(8));
        setAttr("actionUrl", "/");
        setAttr("action", "home");
        render("/common/index.html");
    }
    public void leaveMsg(){
        render("/common/leaveMsg.html");
    }
    public void regist(){
        render("/user/regist.html");
    }
    public void toLogin(){
    	setAttr("msg", "使用邮箱注册作为登陆名");
    	setAttr("color", "");
        render("/user/login.html");
    }
    public void test(){
    	render("/common/test.html");
    }
}
