package cn.iver.controller;

import cn.iver.common.WebUtils;
import cn.iver.interceptor.AdminInterceptor;
import cn.iver.interceptor.LoginInterceptor;
import cn.iver.validator.PostValidator;
import cn.iver.model.Post;
import cn.iver.model.Topic;
import cn.iver.model.User;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;

/**
 * Created with IntelliJ IDEA.
 */
public class PostController extends Controller {
    public void index(){
        int topicID = getParaToInt(0);
        setAttr("userList", User.dao.getUserListbyPoint(8));
        setAttr("topicList", Topic.dao.getHotTopinList(8));
        Page<Post> postPage = Post.dao.getPage(topicID, getParaToInt(1, 1));
        setAttr("postPage", postPage);
        setAttr("topic", Topic.dao.get(topicID));
        render("/post/post.html");
    }

    @Before({LoginInterceptor.class, PostValidator.class})
    public void save(){
    	Post post = getModel(Post.class);
    	User user = WebUtils.currentUser(this);
    	post.set("userID", user.getInt("id")).mySave();
    	redirect("/post/" + post.getInt("topicID"));
    }
    
    @Before({LoginInterceptor.class})
    public void accept(){
    	Post post =  Post.dao.get(getParaToInt(0));
    	System.out.println("当前帖子的积分"+post.getTopic().get("point"));
    	//User user = WebUtils.currentUser(this);
    	User suser = post.getUser();
    	Topic topic = post.getTopic();
    	int oldpoint = suser.get("point");
    	int addpoint = post.getTopic().get("point");
    	int newpoint = oldpoint + addpoint;
    	System.out.println("添加积分前的回复者积分数："+oldpoint);
    	System.out.println("添加积分后的回复者积分数："+oldpoint+addpoint);
    	System.out.println("添加积分后的回复者积分数newpoint："+newpoint);
    	//未回复者添加积分
    	suser.set("point", newpoint).myUpdate();
    	//结贴
    	topic.set("isClose", true).myUpdate();
    	//设置当前评论为最佳答案
    	post.set("accept", true);
    	setAttr("status", 0);
    	setAttr("msg", "操作成功");
    	renderJson();
    }
    //@Before({LoginInterceptor.class})
    public void zan(){
        Post post =  Post.dao.get(getParaToInt(0));
        System.out.println("当前帖子的积分"+post.getTopic().get("point"));
        User user = WebUtils.currentUser(this);
        User suser = post.getUser();
        System.out.println("添加积分前的回复者积分数："+suser.get("point"));
        System.out.println("添加积分后的回复者积分数："+suser.get("point")+post.getTopic().get("point"));
        //post.set("userID", user.getInt("id")).mySave();
        //setAttr("status", 0);
        setAttr("status", 0);
    	setAttr("msg", "操作成功");
    	renderJson();
    }

    @Before(AdminInterceptor.class)
    public void edit(){
        setAttr("post", Post.dao.get(getParaToInt(0)));
        render("/post/edit.html");
    }

    @Before(AdminInterceptor.class)
    public void delete(){
        Post.dao.deleteByID(getParaToInt(0));
        forwardAction("/admin/postList/" + getParaToInt(1));
    }

    @Before({AdminInterceptor.class, PostValidator.class})
    public void update(){
        Post post = getModel(Post.class);
        post.myUpdate();
        redirect("/topic/" + post.get("topicID"));
    }
}
