package cn.iver.controller;

import cn.iver.common.WebUtils;
import cn.iver.interceptor.AdminInterceptor;
import cn.iver.interceptor.LoginInterceptor;
import cn.iver.model.Topic;
import cn.iver.model.User;
import cn.iver.validator.TopicValidator;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

/**
 * Created with IntelliJ IDEA.
 */
public class TopicController extends Controller {
    public void index(){
    	setAttr("actionUrl", "home");
    	setAttr("topicPage", Topic.dao.getPage(8));
    	setAttr("userList", User.dao.getUserListbyPoint(8));
        setAttr("topicList", Topic.dao.getHotTopinList(8));
        forwardAction("/post/" + getParaToInt(0));
    }
    public void module(){
        setAttr("topicPage", Topic.dao.getPageForModule(getParaToInt(0), getParaToInt(1, 1)));
    	setAttr("userList", User.dao.getUserListbyPoint(8));
        setAttr("topicList", Topic.dao.getHotTopinList(8));
        setAttr("actionUrl", "/topic/module/" + getParaToInt(0) + "-");
        render("/common/index.html");
    }
    public void all(){
    	setAttr("topicPage", Topic.dao.getPage(getParaToInt(0, 1)));
    	setAttr("actionUrl", "/topic/all/");
    	setAttr("action", "all");
    	render("/common/list.html");
    }
    public void hot(){
        setAttr("topicPage", Topic.dao.getHotPage(getParaToInt(0, 1)));
        setAttr("actionUrl", "/topic/hot/");
        setAttr("action", "hot");
        render("/common/list.html");
    }
    public void nice(){
    	setAttr("topicPage", Topic.dao.getNicePage(getParaToInt(0, 1)));
    	setAttr("actionUrl", "/topic/nice/");
    	setAttr("action", "nice");
    	render("/common/list.html");
    }
    public void close(){
    	setAttr("topicPage", Topic.dao.getClosePage(getParaToInt(0, 1)));
    	setAttr("actionUrl", "/topic/close/");
    	setAttr("action", "close");
    	render("/common/list.html");
    }
    public void noclose(){
        setAttr("topicPage", Topic.dao.getNoClosePage(getParaToInt(0, 1)));
        setAttr("actionUrl", "/topic/noclose/");
        setAttr("action", "noclose");
        render("/common/list.html");
    }

    @Before(LoginInterceptor.class)
    public void add(){
        render("/topic/add.html");
    }

    @Before({LoginInterceptor.class, TopicValidator.class})
    public void save(){
        Topic topic = getModel(Topic.class);
        User user = WebUtils.currentUser(this);
        topic.set("userID", user.getInt("id")).set("id", null);
        topic.save();
        setAttr("status", 0);
    	setAttr("msg", "操作成功");
    	renderJson();
    }

    @Before(LoginInterceptor.class)
    public void edit(){
        Topic topic = Topic.dao.get(getParaToInt(0));
        setAttr("topic", topic);
        render("/topic/edit.html");
    }

    @Before({LoginInterceptor.class, TopicValidator.class})
    public void update(){
        getModel(Topic.class).myUpdate();
        setAttr("status", 0);
    	setAttr("msg", "操作成功");
    	renderJson();
        //redirect("/post/" + getParaToInt("topic.id"));
    }

    @Before(AdminInterceptor.class)
    public void delete(){
        Topic.dao.deleteByID(getParaToInt(0));
        forwardAction("/admin/topicList/" + getParaToInt(1));
    }
}
