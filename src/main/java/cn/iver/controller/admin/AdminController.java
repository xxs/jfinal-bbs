package cn.iver.controller.admin;

import cn.iver.interceptor.AdminInterceptor;
import cn.iver.model.Post;
import cn.iver.model.Reply;
import cn.iver.model.Topic;
import cn.iver.model.User;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

/**
 * Created with IntelliJ IDEA.
 */
@Before(AdminInterceptor.class)
public class AdminController extends Controller {
    public void index(){
        redirect("/admin/topicList");
    }
    public void topicList(){
        setAttr("topicPage", Topic.dao.getPageForAdmin(getParaToInt(0, 1)));
        render("/admin/topicList.html");
    }
    public void postList(){
        setAttr("postPage", Post.dao.getPageForAdmin(getParaToInt(0, 1)));
        render("/admin/postList.html");
    }
    public void replyList(){
    	setAttr("replyPage", Reply.dao.getPageForAdmin(getParaToInt(0, 1)));
    	render("/admin/replyList.html");
    }
    public void userList(){
        setAttr("userPage", User.dao.getPageForAdmin(getParaToInt(0, 1)));
        render("/admin/userList.html");
    }
    public void editUser(){
    	setAttr("user", User.dao.findById(getParaToInt()));
    	render("/admin/userForm.html");
    }
    public void updateUser(){
    	String id = getPara("id");
    	String rank = getPara("rank");
        String tag = getPara("tag");
    	User user = User.dao.findById(id);
    	user.set("rank", rank);
    	user.set("tag", tag);
        user.myUpdate();
        setAttr("user", user);
        render("/admin/userForm.html");
    }
}
