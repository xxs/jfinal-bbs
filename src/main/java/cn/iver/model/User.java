package cn.iver.model;

import java.util.Date;
import java.util.List;

import cn.iver.common.Const;
import cn.iver.ext.jfinal.Model;

import com.jfinal.kit.HashKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;

/**
 * Created with IntelliJ IDEA.
 * Author: I have A dream
 * Date: 13-3-26
 */
@SuppressWarnings("serial")
public class User extends Model<User> {

    public static final User dao = new User();
    private static final String USER_CACHE = "user";
    private static final String USER_PAGE_FOR_INDEX_CACHE = "userPageForIndex";
    private static final String USER_PAGE_FOR_ADMIN_CACHE = "userPageForAdmin";
    private static final String USER_LIST_CACHE = "userList";
    
    public User() {
        super(USER_CACHE);
    }

    /* get */
    public User get(int id) {
        return loadModel(id);
    }
    public User getByEmailAndPassword(String email, String password){
    	password = HashKit.md5(password);
        return dao.findFirst("select id, username, email, password from user where email=? and password=?", email, password);
    }
    
    public List<User> getUserListbyPoint(int count){
        return dao.findByCache(USER_LIST_CACHE, 1, "select * from user order by point desc limit 0,?",count);
    }
    
    public Page<User> getPageForAdmin(Integer pageNumber) {
    	String cacheName = USER_PAGE_FOR_ADMIN_CACHE;
    	Page<User> topicPage = dao.paginateByCache(cacheName, pageNumber,
    			pageNumber, Const.PAGE_SIZE_FOR_ADMIN,
    			"select id", "from user order by registDate desc");
    	return loadModelPage(topicPage);
    }
    
    public Page<User> getPage(int pageNumber){
        Page<User> userPage = dao.paginateByCache(USER_PAGE_FOR_INDEX_CACHE, pageNumber,
                pageNumber, Const.PAGE_SIZE_FOR_ADMIN,
                "select id", "from user order by registDate desc");
        return loadModelPage(userPage);
    }
    /* other */
    public void mySave(){
        this.filterText("username", "headImg", "blogUrl", "feeling");
        String password = HashKit.md5(this.getStr("password"));
        this.set("password", password).set("registDate", new Date());
        // bug,数据库默认头像没生效
        this.removeNullValueAttrs();
        this.save();
    }

    public void myUpdate() {
        this.filterText("username", "headImg", "blogUrl", "feeling");
        this.update();
        removeThisCache(this.getInt("id"));
    }
    public boolean containEmail(String email) {
        return dao.findFirst("select email from user where email=? limit 1", email) != null;
    }
    public boolean containUsername(String username) {
        return dao.findFirst("select username from user where username=? limit 1", username) != null;
    }
    public boolean containEmailExceptThis(int userID, String email) {
        return dao.findFirst("select email from user where email=? and id!=? limit 1", email, userID) != null;
    }
    public boolean containUsernameExceptThis(int userID, String username) {
        return dao.findFirst("select username from user where username=? and id!=? limit 1", username, userID) != null;
    }

    /* getter */
    public boolean getIsAdmin(String email) {
        return Const.ADMIN_EMAIL.equals(email);
    }

    /* cache */
    private void removeThisCache(int id){
        CacheKit.remove(USER_CACHE, id);
    }
}
