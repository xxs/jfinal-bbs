package cn.iver.common;

import org.apache.commons.lang3.SystemUtils;
import org.beetl.ext.jfinal.BeetlRenderFactory;

import cn.iver.controller.IndexController;
import cn.iver.controller.PostController;
import cn.iver.controller.ReplyController;
import cn.iver.controller.TopicController;
import cn.iver.controller.UserController;
import cn.iver.controller.admin.AdminController;
import cn.iver.controller.admin.ModuleController;
import cn.iver.controller.api.OauthController;
import cn.iver.ext.handler.ViewTestHandler;
import cn.iver.interceptor.GlobalInterceptor;
import cn.iver.model.Module;
import cn.iver.model.Post;
import cn.iver.model.Reply;
import cn.iver.model.Topic;
import cn.iver.model.User;

import com.alibaba.druid.filter.logging.Slf4jLogFilter;
import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;

/**
 * Created with IntelliJ IDEA.
 * 感谢 @波总 的JFinal，@闲.大赋 的beetl，向你们致敬！ ：）
 * 感谢 mike_liu 和 dream_lu，和众群友支持。
 * 如有问题，可以加 JFinal-BBS QQ群：206034609 讨论
 * 【4-3日，1.2版本】更新情况：
 * beetl和的JFinal都升级为最新版；加入了对自定义项目路径的支持；重构了部分代码；
 * 后续未来计划：
 * 取消module，用tag取代他；加入七牛存储支持；升级bootstrap版本；界面的改写；
 */
public class Config extends JFinalConfig {

    private boolean isLocal = isLocal();

    /**
     * 配置常量
     */
    public void configConstant(Constants me) {
        if (isLocal) {
            loadPropertyFile("config-test.txt");
        } else {
            loadPropertyFile("config.txt");
        }
        me.setDevMode(isLocal);

        me.setError404View("/common/404.html");
        me.setError500View("/common/500.html");
        // beetl模版配置工厂
        me.setMainRenderFactory(new BeetlRenderFactory());
    }

    /**
     * 配置路由
     */
    public void configRoute(Routes me) {
        me.add("/", IndexController.class)
          .add("/oauth", OauthController.class)
          .add("/topic", TopicController.class)
          .add("/post", PostController.class)
          .add("/reply", ReplyController.class)
          .add("/user", UserController.class)
          .add("/admin", AdminController.class)
          .add("/admin/module", ModuleController.class);
    }

    /**
     * 配置插件
     */
    public void configPlugin(Plugins me) {
        String jdbcUrl  = getProperty("db.jdbcUrl");
        String user     = getProperty("db.user");
        String password = getProperty("db.password");

        // 配置Druid数据库连接池插件
        DruidPlugin dp = new DruidPlugin(jdbcUrl, user, password);
        dp.addFilter(new StatFilter()).addFilter(new Slf4jLogFilter());
        WallFilter wall = new WallFilter();
        wall.setDbType("mysql");
        dp.addFilter(wall);
        me.add(dp);

        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp).setShowSql(isLocal);
        arp.addMapping("module", Module.class)
           .addMapping("topic", Topic.class)
           .addMapping("post", Post.class)
           .addMapping("reply", Reply.class)
           .addMapping("user", User.class);

        me.add(arp);
        me.add(new EhCachePlugin());
    }

    /**
     * 配置全局拦截器
     */
    public void configInterceptor(Interceptors me) {
        me.add(new GlobalInterceptor());
    }

    /**
     * 配置处理器
     */
    public void configHandler(Handlers me) {
        me.add(new ViewTestHandler());
    }

    /**
     * 初始化常量
     */
    public void afterJFinalStart() { }

    // 判断是否本地，目前为判断的机器的类型
    private boolean isLocal(){
        return !SystemUtils.IS_OS_LINUX;
    }

    public static void main(String[] args) {
        JFinal.start("src/main/webapp", 80, "/", 5);
    }

}