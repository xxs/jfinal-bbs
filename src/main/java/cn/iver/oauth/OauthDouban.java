package cn.iver.oauth;

import java.util.HashMap;
import java.util.Map;

import cn.iver.kit.TokenUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;

/**
 * douban 登录 BAE
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * date Jun 24, 2013 10:18:23 PM
 */
public class OauthDouban extends Oauth {
	private static final String AUTH_URL = "https://www.douban.com/service/auth2/auth";
	private static final String TOKEN_URL = "https://www.douban.com/service/auth2/token";
	private static final String USER_INFO_URL = "https://api.douban.com/v2/user/~me";

	private static OauthDouban oauthDouban = new OauthDouban();

	private OauthDouban(){}
	/**
	 * 用于链式操作
	 * @return OauthDouban
	 */
	public static OauthDouban me() {
		return oauthDouban;
	}

	/**
	 * 获取授权url
	 * @param state OAuth2.0标准协议建议，利用state参数来防止CSRF攻击
	 * @return String 返回类型
	 */
	public String getAuthorizeUrl(String state) {
		return super.getAuthorizeUrl(AUTH_URL, state);
	}

	/**
	 * 获取token
	 * @param code 采用code获取token
	 * @return String 返回类型
	 */
	public String getTokenByCode(String code) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("code", code);
		params.put("client_id", getClientId());
		params.put("client_secret", getClientSecret());
		params.put("grant_type", "authorization_code");
		params.put("redirect_uri", getRedirectUri());
		String token = TokenUtil.getAccessToken(super.doPost(TOKEN_URL, params));
		LOGGER.debug(token);
		return token;
	}

	/**
	 *  获取用户信息
	 * @param accessToken accessToken
	 * @return JSONObject
	 */
	public JSONObject getUserInfo(String accessToken) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("Authorization", "Bearer " + accessToken);
		String userInfo = super.doGetWithHeaders(USER_INFO_URL, params);
		JSONObject dataMap = JSON.parseObject(userInfo);
		LOGGER.debug(dataMap.toJSONString());
		return dataMap;
	}

	/**
	 * 根据code一步获取用户信息
	 * @param code oauth code
	 * @return JSONObject 返回类型
	 */
	public JSONObject getUserInfoByCode(String code) {
		String accessToken = getTokenByCode(code);
		if (StrKit.isBlank(accessToken)) {
			throw new RuntimeException("accessToken is Blank!");
		}
		JSONObject dataMap = getUserInfo(accessToken);
		dataMap.put("access_token", accessToken);
		return dataMap;
	}
	
	@Override
	public Oauth getSelf() {
		return this;
	}
}
