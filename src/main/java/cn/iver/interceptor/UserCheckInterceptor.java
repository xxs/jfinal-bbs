package cn.iver.interceptor;

import cn.iver.common.WebUtils;
import cn.iver.model.User;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

/**
 * Created with IntelliJ IDEA.
 * Author: StevenChow
 * Date: 13-5-7
 */
public class UserCheckInterceptor implements Interceptor {
    @Override
    public void intercept(Invocation inv) {
        Controller controller = inv.getController();

        User user = WebUtils.currentUser(controller);
        System.out.println("userId:"+user.getInt("id"));
        System.out.println("cId:"+controller.getParaToInt(0, 0));
        if (null != user && user.getInt("id") == controller.getParaToInt(0, 0)) {
            inv.invoke();
        }else{
            controller.setAttr("msg", "只有该登录用户本人才有权操作");
            controller.renderError(500);
        }
    }
}
